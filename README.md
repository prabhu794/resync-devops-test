# DevOps Exercise

!!! Please Fork The Code and not push the code to the repository. !!!

This exercise is to test your familiarity with testing, containerizing the application and then set up a continuous Integration (CI) for it using whichever software that you are familar with. 

# Installation
1. Create your own virtual environment.
2. Activate your virtual environment.
3. Install the requirements in the directory pip install -r requirements.txt

# Create the Postgres Database with Docker
With the users.json, create your own postgres container and create your own user table with the users.json. Link the database container with the flask application.

# Run the app
The main app is in the folder called app. The main file is app.py. Basically to run the code `python app/app.py`.
1. The Arithmetic Function is written for you, thus all you need to do is more of testing.
2. The User Class isn't written for you, you need to display knowledge of common REST API knowledge in regards to this. (GET, POST, UPDATE, DELETE). This is done in conjunction with the setup from Postgres DB.

# Testing
1. Build the different unit test for the Arithmetic Function & User Class (Different Functions)

# Containers
After you have done the unit test, we then want you to set up with Docker/any containerization platform that you prefer, but preferably Docker to containerize the flask app. The users need to be able to build this app and run the flask command with Docker.

1. You can use any image base
2. Containerize what you need for the application


# CI 
We want to see during the Continuous Integration, that you understand how CI works before going to the Continuous Delivery/Continuous Deployment phase.

1. The CI must run the test that you have created. You can choose whichever CI system that you prefer. (TravisCI, Jenkins, Gitlab). Use python tests.py for running the test.
2. Dockerfile must have the testing procedure as well as some kind of test for the file you wrote.

If you have a server that you want to use for Continuous Delivery, feel free to go ahead, we do not require you to do CD for this test.


# Sample Request
1. Get User Information
```cURL
curl --location --request GET 'localhost:5000/users?userId=whitewolf' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InByYWJodUBnbWFpbC5jb20iLCJpYXQiOjE2MDUxNTIzMjIsImV4cCI6MTYwNTIzODcyMn0._X-F1rFiDdzLxlcOOrv766MAvzDI87tPIa_Bu_Onea0'
```

2. Get All User Information
```cURL
curl --location --request GET 'localhost:5000/users' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InByYWJodUBnbWFpbC5jb20iLCJpYXQiOjE2MDUxNTIzMjIsImV4cCI6MTYwNTIzODcyMn0._X-F1rFiDdzLxlcOOrv766MAvzDI87tPIa_Bu_Onea0'
```

3. Create User
```cURL
curl --location --request POST 'localhost:5000/users' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InByYWJodUBnbWFpbC5jb20iLCJpYXQiOjE2MDUxNTIzMjIsImV4cCI6MTYwNTIzODcyMn0._X-F1rFiDdzLxlcOOrv766MAvzDI87tPIa_Bu_Onea0' \
--header 'Content-Type: application/json' \
--data-raw '{
        "id": "whitewolf1",
        "name": "Geralt of Rivia",
        "description": "Traveling monster slayer for hire",
        "username": "geralt"
    }'
```

4. Update User
```cURL
curl --location --request PUT 'localhost:5000/users?userId=whitewolf' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InByYWJodUBnbWFpbC5jb20iLCJpYXQiOjE2MDUxNTIzMjIsImV4cCI6MTYwNTIzODcyMn0._X-F1rFiDdzLxlcOOrv766MAvzDI87tPIa_Bu_Onea0' \
--header 'Content-Type: application/json' \
--data-raw '{
        "name": "Geralt of Rivia",
        "description": "Traveling monster slayer for hire",
        "username": "blah"
    }'
```

5. Delete User
```cURL
curl --location --request DELETE 'localhost:5000/users?userId=whitewolf1' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InByYWJodUBnbWFpbC5jb20iLCJpYXQiOjE2MDUxNTIzMjIsImV4cCI6MTYwNTIzODcyMn0._X-F1rFiDdzLxlcOOrv766MAvzDI87tPIa_Bu_Onea0' \
--header 'Content-Type: application/json' \
--data-raw '{
        "name": "Geralt of Rivia",
        "description": "Traveling monster slayer for hire",
        "username": "bbbb"
    }'
```