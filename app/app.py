from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
import json
from app.db.db import PGDB

application = Flask(__name__)
# application.config['BUNDLE_ERRORS'] = True
api = Api(application)
CORS(application, send_wildcard=True)

pgdb = PGDB()

with open("./users.json", "r") as f:
    users = json.load(f)

class arithmetic(Resource):
    def post(self):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('x', type=int, required=True, location='args', help='X Value')
            self.parser.add_argument('y', type=int, required=True, location='args', help='Y Value')
            self.parser.add_argument('operation', type=str, location='args', help='Mathematical Operators')
            self.args = self.parser.parse_args()

            x = (self.args['x'])
            y = (self.args['y'])
            ops = (self.args['operation'])

            if ops == '-':
                return {'results': x - y}
            elif ops == '*':
                return {'results': x * y}
            elif ops == '/':
                return {'results': x / y}
            else:
                return {'results': x + y}

        except Exception as e:
            return {'status': str(e)}

class Users(Resource):
    def format_response(self, data):
        formatted_reponse = []
        for d in data:
            r = {
                "id": d[0],
                "name": d[1],
                "description": d[2],
                "username": d[3]
            }
            formatted_reponse.append(r)
        
        return formatted_reponse

    def get(self):
        ## TODO: Get Users either by id or get all Users
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('userId', type=str, required=False, location='args', help='UserId')
        self.args = self.parser.parse_args()
        data = []
        if 'userId' in self.args and self.args['userId']:
            try:
                user_id = self.args["userId"]
                user = pgdb.get_user_by_id(user_id)
                data.append(user)
            except Exception as e:
                return {"status": e.__str__()}, 400

        else:
            data = pgdb.get_users()

        response = self.format_response(data)
        
        return response
            

    def post(self):
        ## TODO: Create User
        request_body = request.get_json()
        if request_body:
            try:
                user_id = request_body["id"]
                user_name = request_body["name"]
                user_description = request_body["description"]
                username = request_body["username"]

                data = pgdb.check_user_exists(user_id)
                if not data:
                    pgdb.create_user(user_id, user_name, user_description, username)
                    return {"status": "done"}, 200
                else:
                    return {"status": "user already exists"}, 400        
            except Exception as e:
                return {"status": e.__str__()}, 500
            
        else:
            return {"status": "body not passed"}, 400

    def put(self):
        ## TODO: Update User
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('userId', type=str, required=True, location='args', help='UserId')
        self.args = self.parser.parse_args()

        user_id = self.args["userId"]

        request_body = request.get_json()
        if request_body:
            try:
                data = list(pgdb.get_user_by_id(user_id))
                for key in request_body:
                    value = request_body[key]
                    if key == "name":
                        data[1] = value
                    if key == "description":
                        data[2] = value
                    if key == "username":
                        data[3] = value
            
                pgdb.update_user_by_id(user_id, data[1], data[2], data[3])

                return {"status": "done"}, 201
            except Exception as e:
                return {"status": e.__str__()}, 500
        else:
            return {"status": "body not passed"}, 400

    def delete(self):
        ## TODO: Delete User
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('userId', type=str, required=True, location='args', help='UserId')
        self.args = self.parser.parse_args()

        user_id = self.args["userId"]

        try:
            pgdb.get_user_by_id(user_id)
            pgdb.delete_user_by_id(user_id)
            return {"status": "done"}, 200
        except Exception as e:
            return {"status": e.__str__()}, 500

api.add_resource(arithmetic, '/arithmetic')
api.add_resource(Users, '/users')

if __name__ == '__main__':
    application.run(host='0.0.0.0', threaded=True)
