import psycopg2
import os

POSTGRES_USER = os.getenv("POSTGRES_USER", "postgres")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD", "Pass2020!")
POSTGRES_HOST = os.getenv("POSTGRES_HOST", "172.17.0.3")
POSTGRES_PORT = os.getenv("POSTGRES_PORT", "5432")
POSTGRES_DB = os.getenv("POSTGRES_DB", "resync")

class PGDB(object):
    def __init__(self) -> None:
        self.connection = psycopg2.connect(
            user=POSTGRES_USER, 
            password=POSTGRES_PASSWORD, 
            host=POSTGRES_HOST, 
            port=POSTGRES_PORT, 
            database=POSTGRES_DB)

        self.cursor = self.connection.cursor()

    def get_user_by_id(self, user_id):
        self.cursor.execute("Select * from users where id=%s", [user_id])
        record = self.cursor.fetchone()
        if record:
            return record
        else:
            raise Exception("User not found")

    def check_user_exists(self, user_id):
        self.cursor.execute("Select * from users where id=%s", [user_id])
        record = self.cursor.fetchone()
        return record

    def get_users(self):
        self.cursor.execute("Select * from users")
        record = self.cursor.fetchall()
        return record

    def update_user_by_id(self, user_id, user_name, user_descripton, username):
        self.cursor.execute("UPDATE users SET name=%s, description=%s, username=%s where id=%s", [user_name, user_descripton, username, user_id])
        self.connection.commit()

    def create_user(self, user_id, user_name, user_descripton, username):
        self.cursor.execute("INSERT INTO users VALUES (%s, %s, %s, %s)", [user_id, user_name, user_descripton, username])
        self.connection.commit()

    def delete_user_by_id(self, user_id):
        self.cursor.execute("DELETE FROM users where id=%s", [user_id])
        self.connection.commit()