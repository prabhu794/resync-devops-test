FROM alpine:latest

RUN apk add cmd:pip3
RUN apk add --no-cache python3-dev \
    && pip3 install --upgrade pip

RUN apk add postgresql-dev gcc python3-dev musl-dev

WORKDIR /app

COPY . /app

RUN set -e; \
	apk add --no-cache --virtual .build-deps \
		gcc \
		libc-dev \
		linux-headers \
	; \
	pip3 --no-cache-dir install -r requirements.txt; \
	apk del .build-deps;

CMD ["uwsgi", "app/app.ini"]
