import unittest
from app import app
import json

class TestArithematic(unittest.TestCase):

    def setUp(self):
        app.application.testing = True
        self.app = app.application
        self.client = self.app.test_client()

    def tearDown(self):
        del self.app
        del self.client
    
    def test_success_subtraction(self):
        url = "/arithmetic?x=17&y=5&operation=-"
        res = self.client.post(url)
        self.assertTrue(res.status_code, 200)
        self.assertEqual(res.json.get('results', 0), 12)

    def test_success_addition(self):
        url = "/arithmetic?x=17&y=5&operation=+"
        res = self.client.post(url)
        self.assertTrue(res.status_code, 200)
        self.assertEqual(res.json.get('results', 0), 22)
    
    def test_success_multiplication(self):
        url = "/arithmetic?x=10&y=5&operation=*"
        res = self.client.post(url)
        self.assertTrue(res.status_code, 200)
        self.assertEqual(res.json.get('results', 0), 50)

    def test_success_divsion(self):
        url = "/arithmetic?x=10&y=5&operation=/"
        res = self.client.post(url)
        self.assertTrue(res.status_code, 200)
        self.assertEqual(abs(res.json.get('results', 0)), 2.0)

    def test_operator_missing_success(self):
        url = "/arithmetic?x=7&y=5"
        res = self.client.post(url)
        self.assertTrue(res.status_code, 200)
        self.assertEqual(res.json.get('results', 0), 12)
        
    def test_no_parameters_failure(self):
        url = "/arithmetic"
        res = self.client.post(url)
        self.assertTrue(res.status_code, 400)

    def test_string_params_failure(self):
        url = "/arithmetic?x=hello&y=5&operation=+"
        res = self.client.post(url)
        self.assertTrue(res.status_code, 400)
    

class TestUser(unittest.TestCase):
    def setUp(self):
        app.application.testing = True
        self.app = app.application
        self.client = self.app.test_client()

    def tearDown(self):
        del self.app
        del self.client

    def test_success_get_user(self):
        url = "/users?userId=whitewolf"
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json[0]["id"], "whitewolf")
        self.assertEqual(res.json[0]["name"], "Geralt of Rivia")
    
    def test_failure_get_user(self):
        url = "/users?userId=whitewolf5"
        res = self.client.get(url)
        self.assertEqual(res.status_code, 400)

    def test_success_get_users(self):
        url = "/users"
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.json), 4)
    
    def test_success_create_user(self):
        url = "/users"
        data = {
            "id": "prabhu",
            "name": "Prabhu Marappan",
            "description": "Simple Human",
            "username": "prabhu794"
        }
        res = self.client.post(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(res.status_code, 200)
    
    def test_failure_create_user(self):
        url = "/users"
        data = {
            "name": "Prabhu Marappan",
            "description": "Simple Human",
            "username": "prabhu794"
        }
        res = self.client.post(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(res.status_code, 500)

    def test_success_update_user(self):
        url = "/users?userId=prabhu"
        data = {
            "name": "Prabhu Marappan",
            "description": "Simple Human",
            "username": "prabhu7945"
        }
        res = self.client.put(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(res.status_code, 201)

    def test_failure_update_user(self):
        url = "/users?userId=prabhu55"
        data = {
            "name": "Prabhu Marappan",
            "description": "Simple Human",
            "username": "prabhu7945"
        }
        res = self.client.put(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(res.status_code, 500)
    
    def test_failure_update_user(self):
        url = "/users?userId=prabhu55"
        res = self.client.put(url)
        self.assertEqual(res.status_code, 400)
    
    def test_success_delete_user(self):
        url = "/users?userId=m31a3n6sion"
        res = self.client.delete(url)
        self.assertEqual(res.status_code, 200)

if __name__ == '__main__':
    unittest.main()